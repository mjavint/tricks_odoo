# Fechas en odoo

## Ejemplo de comparar fechas en odoo 
```python
from datetime import datetime
from odoo import models, fields, api, _
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import ValidationError
from dateutil.relativedelta import relativedelta


class ModelName(models.Model):
    _name = 'model.name'
    
    date_order = fields.Datetime(string='Date order', required=False)

    @api.constrains('date_order')
    def constrain_date_order(self):
        validate_date_order = datetime.now().date() - relativedelta(days=5)
        for rec in self:
            db_date_order = datetime.strptime(rec.date_order, DEFAULT_SERVER_DATETIME_FORMAT).date()
            if db_date_order < validate_date_order:
                raise ValidationError(u"La fecha del pedido no puede ser menor de 5 dias a la fecha actual")
```


## Trabajo con las fechas por defecto del sistema
```python
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
```
- DEFAULT_SERVER_DATETIME_FORMAT --> "%Y-%m-%d %H:%M:%S"
- DEFAULT_SERVER_DATE_FORMAT ------> "%Y-%m-%d


## Convertir al formato según tu idioma o lenguaje
```python
# date = '2019-06-06'
# record_lang.date_format = '%d/%m/%Y'
lang = self._context.get("lang")
record_lang = self.env["res.lang"].search([("code", "=", lang)], limit=1)
format_date = datetime.strptime(date, DEFAULT_SERVER_DATE_FORMAT).strftime(record_lang.date_format)
```
### Salida
print(format_date)
'06/06/2019'

