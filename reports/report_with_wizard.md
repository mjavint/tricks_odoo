# Reporte con un wizard

## En el wizard `wizard_model.py`
```python
from odoo import models, fields, api, _


class ModelWizard(models.TransientModel):
    _name = 'model.wizard'
    
    name = fields.Char(string='Name', required=False)
    
    @api.multi
    def print_report(self):
        self.ensure_one()
        act_report = self.env.ref(
            'addon_name.report_model').with_context(
                landscape=True).report_action(self, data={'wizard_id': self.id,})

        return act_report
```

## La vista del wizard `wizard_model.xml`
```xml
<odoo>
    <data>
        <record id="view_model_wizard_form" model="ir.ui.view">
            <field name="name">model.wizard.form</field>
            <field name="model">model.wizard</field>
            <field name="type">form</field>
            <field name="arch" type="xml">
                <form string="Kri evolution">
                    <div class="oe_title">
                        <label for="name" class="oe_edit_only"/>
                        <h1>
                            <field name="name" placeholder="......"/>
                        </h1>
                    </div>
                    <footer>
                        <field name="name" invisible="1"/>
                        <button string="Print report" name="print_report" type="object"
                                attrs="{'invisible': [('name', '=', False)]}" class="btn-primary"/>
                        <button string="Cancel" class="btn-secondary" special="cancel"/>
                    </footer>
                </form>
            </field>
        </record>
        <!-- Wizard action -->
        <act_window id="action_wizard_model"
                    name="Model Wizard"
                    res_model="model.wizard"
                    view_type="form"
                    view_mode="form"
                    domain="[]"
                    context="{}"
                    target="new"/>
    </data>
</odoo>
```

## En el report `report_model.py`
```python
from odoo import api, models, fields, _

# Función para redireccionar a una URL después de que se imprima
def generate_url(record, action=None):
    if record:
        record.ensure_one()
        base_url = record.env['ir.config_parameter'].get_param('web.base.url')

        if base_url and base_url[-1:] != '/':
            base_url += '/'

        db = record._cr.dbname

        return "{}web?db={}#id={}&action={}&view_type=form&model={}".format(
            base_url, db, record.id, action or '', record._name
        )
    return ''

class ModelReport(models.AbstractModel):
    _name = 'report.addon_name.model_template'
    
    @api.model
    def _get_report_values(self, docids, data=None):
        self.data = data or {}
        report = self.env['ir.actions.report']._get_report_from_name(
            'addon_name.model_template'
        )
        docids = docids or data.get('ids', [])
        docs = self.env[report.model].browse(docids)
        result = {
            'doc_ids': docids,
            'doc_model': report.model,
            'docs': docs,
            'report_type': self.data.get('report_type') if data else '',
            'get_url': self._get_url,
            'get_data': self._get_data,
            # Otras campos del enterno también se pueden usar
            'company': self.env.user.company_id,
        }
        return result

    def _get_url(self, record):
        return generate_url(record)

    def _get_data(self):
        model_wizard = self.env['model.wizard']
        if self.data.get('wizard_id', False):
            wizard = model_wizard.browse(self.data['wizard_id'])
        return {
            'wizard': wizard,
        }
```

## En la vista del reporte `report_model.xml`
```xml
<?xml version="1.0" encoding="utf-8"?>
<odoo>
    <!-- Paperformats -->
        <record id="proyect_a4_landscape" model="report.paperformat">
            <field name="name">Proyect A4 Landscape</field>
            <field name="default" eval="True"/>
            <field name="format">A4</field>
            <field name="page_height">0</field>
            <field name="page_width">0</field>
            <field name="orientation">Landscape</field>
            <field name="margin_top">5</field>
            <field name="margin_bottom">5</field>
            <field name="margin_left">5</field>
            <field name="margin_right">5</field>
            <field name="header_line" eval="False"/>
            <field name="header_spacing">0</field>
            <field name="dpi">80</field>
        </record>

        <record id="proyect_a4_portrait" model="report.paperformat">
            <field name="name">Proyect A4 Portrait</field>
            <field name="default" eval="True"/>
            <field name="format">A4</field>
            <field name="page_height">0</field>
            <field name="page_width">0</field>
            <field name="orientation">Portrait</field>
            <field name="margin_top">5</field>
            <field name="margin_bottom">5</field>
            <field name="margin_left">5</field>
            <field name="margin_right">5</field>
            <field name="header_line" eval="False"/>
            <field name="header_spacing">0</field>
            <field name="dpi">80</field>
        </record>

    <report id="report_report_model"
                model="model.wizard"
                string="Kri evolution list"
                menu="False"
                report_type="qweb-html"
                name="addon_name.report_template"
                file="addon_name.report_template"/>

    <template id="report_template">
        <t t-call="web.html_container">
            <t t-set="report_data" t-value="get_data()" />
            <t t-call="web.basic_layout">
                <div class="page" style="font-size: 11px !important;">
                    <t t-call="addon_name.render_template" />
                </div>
            </t>
        </t>
    </template>

    <template id="render_template">
        <div class="row mt4">
            <div class="col-sm-9" id="_filters">
              
                <t t-set="wizard" t-value="report_data['wizard']" />

                <div class="row">
                    <div class="col-sm-7">
                        <strong>Name: </strong>
                            <a target="_blank" t-att-href="get_url(record=wizard.name)">
                                <span t-field="wizard.name"/>
                            </a>
                        <br/>
                    </div>
                </div>
            </div>
        </div>
    </template>
</odoo>
```