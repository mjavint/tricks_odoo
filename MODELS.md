# Saber si un usuario pertenece a un grupo

Muchas veces es necesario saber si el usuario logueado pertenece a un determinado grupo para poder ocultar algunos campos o permitirle ejecutar algunas funciones.
Para ello crearemos un campo tipo Boolean, si el usuario pertenece al grupo estará True, de lo contrario estará False.

```python
class ExampleModel(models.Model):
    _name = 'example.model'
    
    is_group_admin = fields.Boolean(
        string='Is Admin',
        compute="_compute_is_group_admin",
    )
    
    @api.one
    def _compute_is_group_admin(self):
        self.is_group_admin = self.env['res.users'].has_group('module_name.group_id_xml')
```

El campo boolean sera computado, la función de dicho campo llama al método has_group de odoo el cual recibe como parámetro el id externo del grupo (este id esta compuesto por el nombre del módulo donde fue creado y el id del grupo , dado en el xml donde fue creado).
